/*
Оголосіть дві змінні: admin та name.
Встановіть ваше ім'я в якості значення змінної name.
Скопіюйте це значення в змінну admin і виведіть його в консоль.
*/
let name;
let admin;
name = "Eugene";
admin = name;
alert(`admin value is ${admin}`);
/*
Оголосити змінну days і ініціалізувати її числом від 1 до 10. 
Перетворіть це число на кількість секунд і виведіть на консоль.
*/
const days = 5;
const seconds = days * 24 * 60 * 60;
alert(`${days} days in seconds equals to ${seconds} seconds`);
/*
Запитайте у користувача якесь значення і виведіть його в консоль.
*/
const answer = prompt(
  "Please enter any text",
  "You do not entered anything, It is default text"
);
alert(`You entered: ${answer}`);
